#!/bin/bash

function checkConnectedDevice
{
    adb get-state
    if [ "$?" -ne 0 ]
    then
        exit 1
    fi
}
function displayWarning
{
    echo "App '$1' is not installed on the device"
}

checkConnectedDevice

declare -a bloatlist=(
    "com.android.browser"
    "com.android.calendar"
    "com.android.deskclock"
    "com.android.mms"
    "com.android.provision"
    "com.android.thememanager"
    "com.android.wallpaperbackup"
    "com.android.xmsf"
    "com.facebook.appmanager"
    "com.facebook.katana"
    "com.facebook.services"
    "com.facebook.system"
    "com.google.android.apps.docs"
    "com.google.android.apps.googleassistant"
    "com.google.android.apps.maps"
    "com.google.android.apps.photos"
    "com.google.android.apps.tachyon"
    "com.google.android.apps.wellbeing"
    "com.google.android.googlequicksearchbox"
    "com.google.android.inputmethod.japanese"
    "com.google.android.inputmethod.latin"
    "com.google.android.music"
    "com.google.android.projection.gearhead"
    "com.google.android.videos"
    "com.google.android.youtube"
    "com.mi.android.globalFileexplorer"
    "com.mi.android.globalminusscreen"
    "com.mi.android.globalpersonalassistant"
    "com.mi.android.thememanager"
    "com.mi.globalTrendNews"
    "com.mipay.wallet.id"
    "com.miui.analytics"
    "com.miui.android.fashiongallery"
    "com.miui.backup"
    "com.miui.bugreport"
    "com.miui.calculator"
    "com.miui.cleanmaster"
    "com.miui.cloudservice"
    "com.miui.cloudservice.sysbase"
    "com.miui.compass"
    "com.miui.daemon"
    "com.miui.gallery"
    "com.miui.hybrid"
    "com.miui.micloudsync"
    "com.miui.miservice"
    "com.miui.msa.global"
    "com.miui.notes"
    "com.miui.player"
    "com.miui.providers.weather"
    "com.miui.screenrecorder"
    "com.miui.securitycore"
    "com.miui.touchassistant"
    "com.miui.translation.kingsoft"
    "com.miui.translationservice"
    "com.miui.videoplayer"
    "com.miui.weather2"
    "com.miui.yellowpage"
    "com.qualcomm.wfd.service"
    "com.touchtype.swiftkey"
    "com.xiaomi.discover"
    "com.xiaomi.joyose"
    "com.xiaomi.mi_connect_service"
    "com.xiaomi.midrop"
    "com.xiaomi.mipicks"
    "com.xiaomi.miplay_client"
    "com.xiaomi.payment"
    "com.xiaomi.scanner"
    "com.xiaomi.xmsf"
)

for I in "${bloatlist[@]}"
do
    adb shell pm uninstall -k --user 0 "$I" || displayWarning "$I"
done

exit