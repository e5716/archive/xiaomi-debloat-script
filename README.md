# Debloating Xiaomi

This is a quick solution to debloat Xiaomi devices. It could work for non-Xiaomi devices too.

I use this approach on a Redmi 4X whose bootloader cannot be unlocked. In any case, is my burner phone so I don't care too much (well...I care a bit. MIUI it's a big spyware monster).

[[_TOC_]]

## Instructions

Run `bash debloat.bash`.

For Windows users, run directly the sources provided on the **Credits** section and try to identify the apps not listed there so you can remove them with the same ADB command.

>**WARNING**: it could remove ALL the keyboards from your phone, so make sure to install a replacement before running this script ([Simple Keyboard](https://f-droid.org/en/packages/com.simplemobiletools.keyboard/) suggested), or just pair a physical Bluetooth keyboard if you can.

If you cannot install any replacement app because the *"INSTALL_FAILED_INTERNAL_ERROR: Permission Denied"* error, turn off the **MIUI Optimization** in Developer settings and reboot the phone.

## Credits

Highly based on, plus a few improvements:

- [MIUI Debloater](https://github.com/ramalhovfc/miui10-11-debloater)
- [MIUI Stock Debloat](https://forum.xda-developers.com/t/guide-debloat-remove-stock-miui-apps-without-root-or-bootloader.3875258/)